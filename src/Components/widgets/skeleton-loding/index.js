import * as React from "react";
import Skeleton from "@mui/material/Skeleton";
import Stack from "@mui/material/Stack";

let SkeletionLoder = function () {
  return (
    <Stack spacing={1}>
      <Skeleton
        variant="rectangular"
        animation="wave"
        maxWidth={345}
        height={200}
      />
      <Skeleton animation="wave" height={70} />
      <Skeleton animation="wave" height={43} width={170} />
    </Stack>
  );
};

export default SkeletionLoder;
