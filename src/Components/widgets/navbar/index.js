import * as React from "react";
import {
  AppBar,
  Avatar,
  Container,
  Typography,
  Toolbar,
  Box,
} from "@mui/material";
import makeStyles from "@material-ui/styles/makeStyles";
import AvatarImage from "../../../assets/logo/Logo1.png";

const useStyles = makeStyles({
  appBar: {
    display: "flex",
    justifyContent: "center",
    height: "52px",
  },
});

const ResponsiveAppBar = (props) => {
  const classes = useStyles(props);

  return (
    <AppBar
      className={classes.appBar}
      height="10"
      color="primary"
      position="sticky"
      elevation={0}
      enableColorOnDark
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box>
            <Avatar alt="Mohit" src={AvatarImage} />
          </Box>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ mr: 2, display: { xs: "none", md: "flex" } }}
          >
            NewsApp
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default ResponsiveAppBar;
