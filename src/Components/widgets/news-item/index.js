import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import CardActionsArea from "@mui/material/CardActionArea";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import makeStyles from "@material-ui/styles/makeStyles";

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
  },
});

let MediaCard = function (props) {
  const classes = useStyles(props);
  return (
    <Card elevation={1} className={classes.card}>
      <CardActionsArea>
        <CardMedia image={props.urlToImage} style={{ height: 200 }} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {props.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {props.description}
          </Typography>
        </CardContent>
      </CardActionsArea>
      <CardActions>
        <Button size="small" variant="contained" href={props.url}>
          Learn More
        </Button>
      </CardActions>
    </Card>
  );
};

export default MediaCard;
