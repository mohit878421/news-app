import Button from "@mui/material/Button";

let ButtonC = function (props) {
  return (
    <>
      <Button disabled={props.disabled} variant={props.variant}>
        {props.text}
      </Button>
    </>
  );
};

export default ButtonC;
