import React from "react";
import Newsitem from "../widgets/news-item";
import Button from "../widgets/button";
import SkeletionLoder from "../widgets/skeleton-loding";
import { countries, categorys } from "../../assets/dummy-data/news-data";
import {
  Container,
  Box,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
} from "@mui/material";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

let News = function (props) {
  let [totalFetchNews, setTotalFetchNews] = React.useState(0);
  let [totalNewsPerPage, setTotalNewsPerPage] = React.useState(9);
  let [newsData, setNewsData] = React.useState([]);
  let [page, setPage] = React.useState(1);
  let [next, setNext] = React.useState(false);
  let [loder, setLoder] = React.useState(false);
  let [country, setCountry] = React.useState("br"); //???
  let [category, setCategory] = React.useState("health");
  const changeCountry = (event) => {
    setCountry(event.target.value);
  };
  const changeCategory = function (event) {
    setCategory(event.target.value);
  };
  let Previous = function () {
    page -= 1;
    setPage(page);
    setNext(false);
  };

  let Next = function () {
    page += 1;
    setPage(page);
    if (page >= Math.ceil(totalFetchNews / totalNewsPerPage)) {
      setNext(true);
    }
  };
  React.useEffect(() => {
    // debugger;
    setLoder(true);
    axios
      .get(
        // "https://saurav.tech/NewsAPI/top-headlines/category/health/in.json"
        `https://newsapi.org/v2/top-headlines?country=${country[0]}${country[1]}&category=${category}&apiKey=a15347a9a22849d5b1a7a1bcb81a3ecd&page=${page}&pageSize=${totalNewsPerPage}`
      )
      .then((res) => {
        setTotalFetchNews(res.data.totalResults);
        setNewsData(res.data.articles);
        setLoder(false);
        console.log(res.data.articles);
      })
      .catch((error) => console.log(error));
  }, [page, category, country, totalNewsPerPage]);
  return (
    <Container>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <h2 className="text-center">NewsMonkey- Top Headlines</h2>
      </Box>
      <Grid container justifyContent="center" spacing={3}>
        <Grid item lg={4} md={6} sm={6}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Country</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={country}
              label="country"
              onChange={changeCountry}
            >
              {countries.map((c) => (
                <MenuItem value={c}>{c}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item lg={4} md={6} sm={6}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={category}
              label="category"
              onChange={changeCategory}
            >
              {categorys.map((c) => (
                <MenuItem value={c}>{c}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
      <Grid container justifyContent="center" spacing={3}>
        {newsData.map((val, index) => {
          return (
            <React.Fragment key={index}>
              <Grid item lg={4} md={6} sm={6}>
                {loder == false ? (
                  <Newsitem
                    title={val.title}
                    description={val.description}
                    urlToImage={val.urlToImage}
                    url={val.url}
                  />
                ) : (
                  <SkeletionLoder />
                )}
              </Grid>
            </React.Fragment>
          );
        })}
      </Grid>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          p: 1,
          m: 1,
          bgcolor: "background.paper",
        }}
      >
        <Button
          disabled={page <= 1}
          variant="contained"
          text="Previous"
          onClick={Previous}
        />
        {/* <Button disabled={page <= 1} variant="contained" onClick={Previous}>
          Previous
        </Button> */}
        <Button
          disabled={next}
          variant="contained"
          onClick={Next}
          text="Next"
        />
        {/* <Button disabled={next} variant="contained" onClick={Next}>
          Next
        </Button> */}
      </Box>
    </Container>
  );
};
export default News;
