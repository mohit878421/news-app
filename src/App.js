import React from "react";
import Navbar from "./Components/widgets/navbar/index";
import News from "./Components/shared/news";

let App = function () {
  return (
    <div>
      <Navbar />
      <News />
    </div>
  );
};

export default App;
